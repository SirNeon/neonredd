# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'redd/version'

# rubocop:disable Metrics/LineLength
Gem::Specification.new do |spec|
  spec.name          = 'neonredd'
  spec.version       = Redd::VERSION
  spec.authors       = ['SirNeon']
  spec.email         = ['sirneon618@gmail.com']
  spec.summary       = 'A Reddit API Wrapper for Ruby.'
  spec.description   = 'This is literally just Redd that can grab up to 1000 listing items.'
  spec.homepage      = 'https://gitlab.com/SirNeon/neonredd'
  spec.license       = 'MIT'
  spec.required_ruby_version = '>= 2.1.0'
  # rubocop:disable Style/RegexpLiteral
  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(/^bin\//) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(/^(test|spec|features)\//)
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.10.6'
  spec.add_development_dependency 'rake', '~> 10.4.2'
  spec.add_development_dependency 'rspec', '~> 3.2.0'
  spec.add_development_dependency 'vcr', '~> 2.9.3'
  spec.add_development_dependency 'webmock', '~> 1.20.4'

  spec.add_dependency 'hashie', '~> 3.4.0'
  spec.add_dependency 'faraday', '~> 0.9.1'
  spec.add_dependency 'multi_json', '~> 1.10.1'
  spec.add_dependency 'fastimage', '~> 1.6.6'
end
