# 0.0.5

* Fixed a bug where the API connection wouldn't be reset after
refreshing an access token.

# 0.0.4

* Bug fixed where 1 too many items would be added to a listing.
